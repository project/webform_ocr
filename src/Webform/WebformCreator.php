<?php
namespace Drupal\webform_ocr\Webform;

use Drupal\webform\Entity\Webform;
use Drupal\webform\WebformInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides responses for frontpage route.
 */
class WebformCreator  {

  public function create($title, $strings) {
    // Define OCR Debris to be removed.
    $debris = ['_', '-', ' '];
    // Iterate received info into new Webform fields.
    foreach ($strings as $key => $value) {
      // Insert only wanted values in the array.
      if(!empty($value) && !in_array($value, $debris)) {
        // Machine name for input field.
        $machine_name = preg_replace('@[^a-z0-9_]+@','_', strtolower($value));
        // The input textfield.
        $elements[] = [
            $machine_name => [
            '#type' => 'textfield',
            '#title' => $value,
          ]
        ];
      }
    }

    // Create the Webform.
    $title_machine_name = preg_replace('@[^a-z0-9_]+@','_', strtolower($title));
    $webform = Webform::create( [
        'langcode' => 'en',
        'status' => WebformInterface::STATUS_OPEN,
        'id' => $title_machine_name,
        'title' => $title,
        'elements' => Yaml::encode($elements),
        'settings' => Webform::getDefaultSettings(),
      ]);
    $webform->save();
    // Return Webform URL.
    return $webform->toUrl();
  }
  
}
