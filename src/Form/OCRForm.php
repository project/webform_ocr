<?php

namespace Drupal\webform_ocr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\webform_ocr\Webform\WebformCreator;
use Drupal\webform_ocr\OCR\Tesseract;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Defines a form to input files to be recognized by Tesseract.
 */
class OCRForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_ocr_new_ocr';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_ocr.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Accepted formats
    $formats = 'png gif jpg jpeg';
    // Fields
    $form['webform_ocr_title'] = array(
      '#title' => t('Webform Title'),
      '#type' => 'textfield',
      '#required' => TRUE
    );
    $form['webform_ocr_file'] = array(
      '#title' => t('Image to OCR'),
      '#type' => 'file'
    );
    $form['webform_ocr_file'] = array(
      '#type'          => 'managed_file',
      '#title'         => t('Choose documents to OCR'),
      '#upload_location' => 'public://images/',
      '#default_value' => '',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#states'        => array(
        'visible'      => array(
          ':input[name="image_type"]' => array('value' => t('Upload your file(s)')),
        ),
      ),
      '#upload_validators' => array(
      'file_validate_extensions' => array($formats),
      ),
      '#description' => t('Allowed extensions: ' . $formats),
  	);
    $form['webform_ocr_strings'] = array(
      '#type' => 'hidden'
    );
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Create Webform'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Form State Data - Title
    $title = $form_state->getValue('webform_ocr_title');

    // Check for an existing Webform with same title machine_name.
    if(!empty($title)) {
      $title_machine_name = preg_replace('@[^a-z0-9_]+@','_', strtolower($title));
      $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($title_machine_name);
      if(!is_null($webform)){
        // Set Error
        $form_state->setErrorByName('webform_ocr_title', $this->t('Webform ' . $title_machine_name . ' already exists.'));
      }
    }
    // Form State Data - Files
    foreach ($form_state->getValue('webform_ocr_file') as $file) {
      $file_entities[] = File::load($file);
    }
    // Iterate in case of multiple files
    foreach ($file_entities as $file) {
      $file_path = \Drupal::service('file_system')->realpath($file->getFileUri());
      // OCR received images
      $tesseract = new Tesseract();
      $tesseract = $tesseract->OCR($file_path);
      $ocr[] = explode("\n", $tesseract);
    }
    // Check for empty OCR
    if(!isset($ocr) || empty($ocr[0][0])) {
      // Set Error
      $form_state->setErrorByName('webform_ocr_file', $this->t('No text could be recognized in the uploaded file.'));
    }

    $ocr = array_reduce($ocr, 'array_merge', array());
    $form_state->setValue('webform_ocr_strings', $ocr);


  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Files
    foreach ($form_state->getValue('webform_ocr_file') as $file) {
      $file_entities[] = File::load($file);
    }
    // Create Webform.
    $webformcreator = new WebformCreator();
    $webformcreator = $webformcreator->create($form_state->getValue('webform_ocr_title'), $form_state->getValue('webform_ocr_strings'));
    // Set URL Redirect
    $form_state->setRedirectUrl($webformcreator);
    // Delete files after the webform has been created.
    foreach ($file_entities as $file) {
      $file->delete();
    }
  }

}
