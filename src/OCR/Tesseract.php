<?php

namespace Drupal\webform_ocr\OCR;

use thiagoalessio\TesseractOCR\TesseractOCR;

/**
 * Defines a class to OCR files using Tesseract.
 */
class Tesseract  {

  public function OCR($file) {
    // Tesseract the received file
    $tesseract = new TesseractOCR($file);
    
    return $tesseract->run();
  }

}
