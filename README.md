# Webform OCR 1.x

OCR images into new webform.

## USAGE

1. Enable module
2. Go to /admin/structure/webform
3. Click 'OCR image' link action
4. Define a webform Title
5. Pick desired images to be 'OCRed'

## REQUIRED
thiagoalessio/tesseract_ocr

Can be manually required as "composer require thiagoalessio/tesseract_ocr" or
"composer install" inside the webform_ocr folder
